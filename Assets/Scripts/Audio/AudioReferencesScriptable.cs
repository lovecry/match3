﻿/// <summary>
/// Hold audio references
/// </summary>
/// <remarks>
/// Use this scriptable to hold references ao audio assets, these will be accessible for audio manager
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "CreateAsset/AudioReferencesScriptable", fileName = "AudioReferencesScriptable")]
public class AudioReferencesScriptable : ScriptableObject
{
    [SerializeField] AudioClip m_backgroundMenu;
    [SerializeField] AudioClip m_backgroundGame;
    [SerializeField] AudioClip m_jewelDestruction;
    [SerializeField] AudioClip m_jewelSwap;
    [SerializeField] AudioClip m_buttonPressed;

    //getters
    public AudioClip BackgroundMenu { get { return m_backgroundMenu; } }
    public AudioClip BackgroundGame { get { return m_backgroundGame; } }
    public AudioClip JewelDestruction { get { return m_jewelDestruction; } }
    public AudioClip JewelSwap { get { return m_jewelSwap; } }
    public AudioClip ButtonPressed { get { return m_buttonPressed; } }
}
