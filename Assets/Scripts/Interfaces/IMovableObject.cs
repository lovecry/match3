﻿/// <summary>
/// Interface for movable objects
/// </summary>
/// <remarks>
/// Use this interface for object that is supposed to be moved, you can check if are moving or not
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IMovableObject {

    //Set this bool to true when object is moving and false when it finished moving
    bool IsMoving { get; set; }
}
