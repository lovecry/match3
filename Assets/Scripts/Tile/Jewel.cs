﻿/// <summary>
/// Class to store jewe state
/// </summary>
/// <remarks>
/// This class is used store and manage functionality of specified TileableItemBase, the Jewel
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Jewel : TileableItemBase, IMovableObject
{
    //Used to define wich sprite use for the tile
    public enum ColorType 
    {
        UNDEFINED = -1,
        BLUE = 0,
        GREEN = 1,
        PURPLE = 2,
        RED = 3,
        YELLOW = 4
    }

    [SerializeField] SpriteRenderer m_spriteRenderer;
    [SerializeField] TextMesh m_text;
    [SerializeField] ScaleAnimator m_scaleAnimator;

    //Direct correspondence between this array order and ColorType enum order to improve semplicity and performance
    [Header("Resources")]
    [SerializeField] Sprite[] m_jewelSprites; 

    ColorType m_jemColor = ColorType.UNDEFINED;
    public ColorType Color { get { return m_jemColor; } }

    bool m_isMoving = false;
    bool m_isDestroying = false;
    public bool IsDestroying { get { return m_isDestroying; } }

    public bool IsMoving
    {
        get { return m_isMoving; }
        set { m_isMoving = value; }
    }

    private void Start()
    {
        enabled = false;
#if DEBUG_TILES
        enabled = true;
        m_text.gameObject.SetActive(true);
#endif
    }

    private void Update()
    {
#if DEBUG_TILES
        m_text.text = m_rowPosition + " " + m_columnPosition;
#endif
    }

    #region Public Interface
    public void SetColor(ColorType colorType)
    {
        m_jemColor = colorType;
        m_spriteRenderer.sprite = m_jewelSprites[(int)colorType];
    }

    public void StartDestroyAnimation(System.Action onAnimationEnded)
    {
        m_isDestroying = true;
        m_scaleAnimator.Play(false, onAnimationEnded);
    }

    public override void Dispose()
    {
        base.Dispose();
        m_scaleAnimator.Reset();
        m_jemColor = ColorType.UNDEFINED;
        m_isMoving = false;
        m_isDestroying = false;
    }
    #endregion
}
