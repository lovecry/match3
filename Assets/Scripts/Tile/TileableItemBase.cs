﻿/// <summary>
/// Base Class for items used in board manager
/// </summary>
/// <remarks>
/// This class is used store and manage common functionality for items used in board manager
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileableItemBase : MonoBehaviour, System.IDisposable {

    //Input event, when pointer enter and exit tile
    public event System.Action<int, int> OnPointerExit;
    public event System.Action<int, int> OnPointerEnter;

    protected int m_rowPosition = -1; // -1 is a sort of invalid index
    public int RowIdx { get { return m_rowPosition; } }
    protected int m_columnPosition = -1; // -1 is a sort of invalid index
    public int ColumnIdx { get { return m_columnPosition; } }

    #region public interface
    public void SetItemPosition(int rowIdx, int columnIdx)
    {
        m_rowPosition = rowIdx;
        m_columnPosition = columnIdx;
    }
    #endregion

    #region Callbacks
    public void OnMouseEnter()
    {
        if (OnPointerEnter != null)
            OnPointerEnter(m_rowPosition, m_columnPosition);
    }

    public void OnMouseExit()
    {
        if (OnPointerExit != null)
            OnPointerExit(m_rowPosition, m_columnPosition);
    }
    
    public virtual void Dispose()
    {
        m_rowPosition = -1;
        m_columnPosition = -1;
        OnPointerExit = null;
        OnPointerEnter = null;
    }
    #endregion
}
