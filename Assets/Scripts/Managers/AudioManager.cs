﻿/// <summary>
/// Manager class for audio
/// </summary>
/// <remarks>
/// Manager class that holds references to audioclips and audiosources and wrap the use of AudioSource
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManager : Singleton<AudioManager> {

    [SerializeField] AudioSource m_soundtrackAudioSource;
    [SerializeField] AudioSource m_FXAudioSource;
    [SerializeField] AudioReferencesScriptable m_audioReferences;

    public AudioReferencesScriptable AudioClips { get { return m_audioReferences; } }

    //Play persistent AudioClips
    public void Play(AudioClip audioClip)
    {
        m_soundtrackAudioSource.clip = audioClip;
        m_soundtrackAudioSource.Play();
    }

    //Play oneshot AudioClips like FX sounds
    public void PlayOneShot(AudioClip audioClip)
    {
        m_FXAudioSource.PlayOneShot(audioClip);
    }
}
