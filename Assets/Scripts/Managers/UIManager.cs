﻿/// <summary>
/// Central class manager for UI
/// </summary>
/// <remarks>
/// This class is used to manage all modification for the UI, this manager holds all UI referances and Wrap ui functionality
/// </remarks>

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : Singleton<UIManager>
{
    //UI Events
    public event System.Action m_onMainMenuExitButtonPressed;
    public event System.Action m_onMainMenuEnterButtonPressed;
    public event System.Action m_onInGameMenuExitButtonPressed;

    MainMenuUI m_mainMenuUI;
    InGameUI m_inGameUI;

    // Use this for initialization
    void Awake ()
    {
        m_instance = this;
	}

    #region Initialization
    void Start()
    {
        FindUiReferences();
    }

    //After scene is loaded, we need to catch up right ui reference and initialize it
    public void SceneLoaded()
    {
        FindUiReferences();
    }

    private void FindUiReferences()
    {
        m_mainMenuUI = FindObjectOfType<MainMenuUI>();
        if (m_mainMenuUI != null)
        {
            m_mainMenuUI.Initialize(OnMainMenuExitButtonPressed, OnMainMenuEnterButtonPressed);

        }
        m_inGameUI = FindObjectOfType<InGameUI>();
        if (m_inGameUI != null)
        {
            m_inGameUI.Initialize(OnInGameExitButtonPressed);
        }
    }
    #endregion

    #region Update UI elements
    //Update UI methods
    public void UpdateScoreUI(int currentScore)
    {
        if (m_inGameUI != null)
        {
            m_inGameUI.SetScore(currentScore);
        }
    }

    public void UpdateInGameTimer(float currentTime)
    {
        if (m_inGameUI != null)
        {
            m_inGameUI.SetTimerText(currentTime);
        }
    }

    public void UpdateFullScreenTimer(float currentTime)
    {
        if (m_inGameUI != null)
        {
            m_inGameUI.SetTimerBeforeStartText(currentTime);
        }
    }

    public void SetFullScreenTimerActive(bool value)
    {
        if (m_inGameUI != null)
        {
            m_inGameUI.SetTimerFullScreenActive(value);
        }
    }

    public void SetGameOverActive(bool value)
    {
        if (m_inGameUI != null)
        {
            m_inGameUI.SetGameOverActive(value);
        }
    }
    #endregion

    #region callbacks
    //Redirect callbacks for input events
    private void OnInGameExitButtonPressed()
    {
        if (m_onInGameMenuExitButtonPressed != null)
            m_onInGameMenuExitButtonPressed();
    }

    private void OnMainMenuExitButtonPressed()
    {
        if (m_onMainMenuExitButtonPressed != null)
            m_onMainMenuExitButtonPressed();
    }

    private void OnMainMenuEnterButtonPressed()
    {
        if (m_onMainMenuEnterButtonPressed != null)
            m_onMainMenuEnterButtonPressed();
    }
    #endregion
}
