﻿/// <summary>
/// Manager class used to manage logic of a generic board
/// </summary>
/// <remarks>
/// This class is used to manage all board utils like generate, swap command, destroy item etc....
/// You can generate items using your prefab or no prefab or a pool
/// This is a singleton since we need only one board.
/// </remarks>

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : Singleton<BoardManager> {

    //Boardmanager is the one who notify outside if a tile in the board is selected or a pointer is over it, and board refill events
    public event System.Action<int, int> m_onPointerExitedTile;
    public event System.Action<int, int> m_onPointerEnteredTile;
    public event System.Action<int, int> m_onItemRefilled;
    public event System.Action m_onBoardFullRefilled;

    [SerializeField] GameObject m_boardContainer;
    [SerializeField] ObjectsMover m_objectMover;
    [SerializeField] float m_delayAtBoardRefill = 0.2f;

    GameObject[,] m_tilesMatrixGO;
    //Get all tiles of the board
    public GameObject[,] Tiles { get { return m_tilesMatrixGO; } } 

    //Hold position of tiles after initialization to easy found a position if tile in that Row, Column is missing
    Vector3[,] m_boardTilePositions;
    // Offset between tiles
    Vector2 m_tileOffset = Vector2.zero; 

    int m_boardRowsCount = 0;
    public int RowsCount { get { return m_boardRowsCount; } }
    int m_boardColumnsCount = 0;
    public int ColumnsCount { get { return m_boardColumnsCount; } }

    //Pool reference or tile reference
    ObjectsPool<GameObject, GameObjectsPoolAllocator> m_tileableItemsPool = null;
    GameObject m_tile;

    void Awake () {
        m_instance = this;
    }

    #region Public Interface
    //Entry points for board generator, 3 overloads, 
    // 1) With just specify Row and Colmns
    // 2) Use an object pool
    // 3) Use a prefab reference
    public void GenerateBoard(int rows, int columns)
    {
        InitBoard(rows, columns);
        GenerateTiles();
    }

    public void GenerateBoard(int rows, int columns, Vector2 tileOffset = default(Vector2), ObjectsPool<GameObject, GameObjectsPoolAllocator> objectPool = null)
    {
        m_tileableItemsPool = objectPool;
        InitBoard(rows, columns);
        GenerateTiles(objectPool, tileOffset);
    }

    public void GenerateBoard(int rows, int columns, Vector2 tileOffset = default(Vector2), GameObject tile = null)
    {
        m_tile = tile;
        InitBoard(rows, columns);
        GenerateTiles(tile, tileOffset);
    }

    //Utils methods for board management
    //Check if row and column idx are inside a board
    public bool IsInsideTheBoard(int rowIdx, int columnIdx)
    {
        return (rowIdx >= 0 && rowIdx < m_boardRowsCount) && (columnIdx >= 0 && columnIdx < m_boardColumnsCount);
    }

    //Get a tile if it's inside the board
    public GameObject GetTile(int rowIdx, int columnIdx)
    {
        if (IsInsideTheBoard(rowIdx, columnIdx))
        {
            return m_tilesMatrixGO[rowIdx, columnIdx];
        }
        return null;
    }

    //Remove a tile from board at specified row, column
    public void RemoveTileFromBoard(int rowIdx, int columnIdx)
    {
        if (!IsInsideTheBoard(rowIdx, columnIdx))
        {
            return;
        }
        if (m_tileableItemsPool != null) //Use pool
        {
            if (m_tilesMatrixGO[rowIdx, columnIdx] != null)
            {
                m_tileableItemsPool.DisposeComponentAndDestroy<TileableItemBase>(m_tilesMatrixGO[rowIdx, columnIdx].GetComponent<TileableItemBase>()); //Return to pool after dispose
            }
        }
        else //use brutal destroy
        {
            GameObject.Destroy(m_tilesMatrixGO[rowIdx, columnIdx]); 
        }
        m_tilesMatrixGO[rowIdx, columnIdx] = null;
    }

    //Swap two tiles in the board
    public void SwapTiles(int tile1Row, int tile1Col, int tile2Row, int tile2Col, System.Action<TileableItemBase, TileableItemBase> onSwapEnded)
    {
        //Protection, can't swap if row column is not in the board
        GameObject tile1 = IsInsideTheBoard(tile1Row, tile1Col) ? m_tilesMatrixGO[tile1Row, tile1Col] : null;
        GameObject tile2 = IsInsideTheBoard(tile2Row, tile2Col) ? m_tilesMatrixGO[tile2Row, tile2Col] : null;

        //Can't swap if no tile to swap
        if (tile1 == null || tile2 == null)
        {
            if (onSwapEnded != null)
                onSwapEnded(null, null);
            return;
        }

        //Use object mover to move and swap tiles, then call a callback
        m_objectMover.SwapTwoObjects(tile1, tile2, () => 
        {
            GameObject tmp = m_tilesMatrixGO[tile1Row, tile1Col];
            m_tilesMatrixGO[tile1Row, tile1Col] = m_tilesMatrixGO[tile2Row, tile2Col];
            m_tilesMatrixGO[tile2Row, tile2Col] = tmp;

            //We don't need protection of tileableItemBase coz boardmanager add this component at board generation
            TileableItemBase item1Component = m_tilesMatrixGO[tile1Row, tile1Col].GetComponent<TileableItemBase>();
            item1Component.SetItemPosition(tile1Row, tile1Col);
            TileableItemBase item2Component = m_tilesMatrixGO[tile2Row, tile2Col].GetComponent<TileableItemBase>();
            item2Component.SetItemPosition(tile2Row, tile2Col);

            //Callback on swap ended
            if (onSwapEnded != null)
                onSwapEnded(item1Component, item2Component);
        });
    }

    //Collapse column, tile upon the hole will falling down by 1 position
    public void CollapseColumn(int columnIdx)
    {
        //Check for holes
        for (int i = 0 ; i < m_boardRowsCount - 1; ++i)
        {
            if (m_tilesMatrixGO[i, columnIdx] == null)
            {
                //Hole found, go upon position and check first tile
                int holeIdx = i;
                for (int j = i + 1; j < m_boardRowsCount; ++j)
                {
                    if (m_tilesMatrixGO[j, columnIdx] != null)
                    {
                        //Tile found, fall down tile by 1 position (swap hole with tile found)
                        int itemRowIdx = holeIdx; //this is because when use lambda function, we don't know when that function is called and variable from outside can change, we need a copy of it
                        m_objectMover.MoveObj(m_tilesMatrixGO[j, columnIdx], m_boardTilePositions[holeIdx, columnIdx], () => { OnItemRefilled(itemRowIdx, columnIdx); });
                        m_tilesMatrixGO[holeIdx, columnIdx] = m_tilesMatrixGO[j, columnIdx];
                        m_tilesMatrixGO[holeIdx, columnIdx].GetComponent<TileableItemBase>().SetItemPosition(holeIdx, columnIdx);
                        m_tilesMatrixGO[j, columnIdx] = null;
                        holeIdx++;
                    }
                }
            }
        }
    }

    //Collapse all board by collapse all columns
    public void CollapseBoard()
    {
        for (int i = 0; i < m_boardColumnsCount; ++i)
        {
            CollapseColumn(i);
        }
    }

    //Refill the board
    public List<GameObject> RefillBoard()
    {
        List<GameObject> objectsForRefillList = new List<GameObject>(); //We need a list of Tiles to allow from outside to initialize tiles used for refill
        float delayTime = 0;
        for (int i = 0; i < m_boardRowsCount; ++i)
        {
            bool holeFound = false;
            for (int j = 0; j < m_boardColumnsCount; ++j)
            {
                if (m_tilesMatrixGO[i, j] == null)
                {
                    //Here hole is found
                    holeFound = true;
                    GameObject newTile = null; //hold reference of a tile used for refill...
                    if (m_tileableItemsPool != null) //.... if using pool
                    {
                        newTile = m_tileableItemsPool.Instantiate();
                    }
                    else //.... or using new gameobject
                    {
                        newTile = GameObject.Instantiate(m_tile != null ? m_tile : new GameObject());
                    }
                    newTile.transform.position = new Vector3(m_boardTilePositions[i, j].x, m_tileOffset.y * (m_boardRowsCount), 0); //Use right position, outside the board
                    newTile.transform.SetParent(m_boardContainer != null ? m_boardContainer.transform : null);

                    //Same as above, using lambda function
                    int itemRowIdx = i; 
                    int itemColumnIdx = j;

                    // move the gameobject but after a delay
                    m_objectMover.MoveObjAfterDelay(newTile, m_boardTilePositions[i, j], delayTime, () => { OnItemRefilled(itemRowIdx, itemColumnIdx);  }, 3.0f);

                    //Update the board structure with new tile
                    AddTileToBoard(newTile, i, j);
                    objectsForRefillList.Add(newTile);
                }
            }
            if (holeFound)
            {
                delayTime += m_delayAtBoardRefill; //Increase delay if we are refilling another row
            }
        }
        return objectsForRefillList;
    }
    #endregion

    #region InitializationMethods
    //Initialize board, common initialization for 3 entry points
    void InitBoard(int rows, int columns)
    {
        m_boardRowsCount = rows;
        m_boardColumnsCount = columns;
        m_tilesMatrixGO = new GameObject[rows, columns];
        m_boardTilePositions = new Vector3[rows, columns];
        m_objectMover.m_onAllMovementEnded += OnBoardFullRefilled;
    }

    //Tile generation using pool
    void GenerateTiles(ObjectsPool<GameObject, GameObjectsPoolAllocator> objectPool, Vector2 tileSpacing = default(Vector2))
    {
        Vector2 startingPoint = CalculateTileStartingPoint(m_boardContainer);
        for (int x = 0; x < m_boardRowsCount; x++)
        {
            for (int y = 0; y < m_boardColumnsCount; y++)
            {
                GameObject newTile = objectPool.Instantiate();
                //Set new tile right position
                Vector2 tilesSpacing = CalculateTilesSpacing(newTile, tileSpacing);
                newTile.transform.SetParent(m_boardContainer != null ? m_boardContainer.transform : null);
                newTile.transform.position = new Vector3(startingPoint.x + (tilesSpacing.x * y), startingPoint.y + (tilesSpacing.y * x), 0);
                newTile.transform.localPosition = new Vector3(newTile.transform.localPosition.x, newTile.transform.localPosition.y, 0);
                //Update the board structure
                m_boardTilePositions[x, y] = newTile.transform.position;
                AddTileToBoard(newTile, x, y);
            }
        }
        //Board is initialized, so it is fully refilled first time
        OnBoardFullRefilled();
    }

    //Tile generation using gameobject reference
    void GenerateTiles(GameObject tile = null, Vector2 tileSpacing = default(Vector2))
    {
        if (tile == null)
        {
            tile = new GameObject();
        }
        Vector2 startingPoint = CalculateTileStartingPoint(m_boardContainer);
        Vector2 tilesSpacing = CalculateTilesSpacing(tile, tileSpacing);
        for (int x = 0; x < m_boardColumnsCount; x++)
        {
            for (int y = 0; y < m_boardRowsCount; y++)
            {
                //Instantiate new gameobject and put in a right position
                GameObject newTile = Instantiate
                    (
                    tile,
                    new Vector3(startingPoint.x + (tilesSpacing.x * y), startingPoint.y + (tilesSpacing.y * x), 0),
                    tile.transform.rotation,
                    m_boardContainer != null ? m_boardContainer.transform : null
                    );
                newTile.transform.localPosition = new Vector3(newTile.transform.localPosition.x, newTile.transform.localPosition.y, 0);
                //Update the board structure
                m_boardTilePositions[x, y] = newTile.transform.position;
                AddTileToBoard(newTile, x, y);
            }
        }
        //Board is initialized, so it is fully refilled first time
        OnBoardFullRefilled();
    }

    //Tile starting point depend if there is a container for board or not
    Vector2 CalculateTileStartingPoint(GameObject boardContainer = null)
    {
        return boardContainer != null ? new Vector2(boardContainer.transform.position.x, boardContainer.transform.position.y) : Vector2.zero;
    }

    //Calculate tile spacing using custom offset we provided and sprite renderer bound if there is one
    Vector2 CalculateTilesSpacing(GameObject tile, Vector2 tileOffset)
    {
        SpriteRenderer sprite = tile.GetComponent<SpriteRenderer>();
        Vector2 spriteBounds = sprite != null ? new Vector2(sprite.bounds.size.x, sprite.bounds.size.y) : Vector2.zero;
        Vector2 offset = spriteBounds + tileOffset;
        m_tileOffset = offset;
        return offset;
    }

    //Common initialization for tile added at board structure
    void AddTileToBoard(GameObject tileGO, int rowIdx, int columnIdx)
    {
        //add a TileableComponent if there is not
        TileableItemBase tileableComponent = tileGO.GetComponent<TileableItemBase>();
        if (tileableComponent == null)
        {
            tileableComponent = tileGO.AddComponent<TileableItemBase>();
        }

        tileableComponent.SetItemPosition(rowIdx, columnIdx);
        tileableComponent.OnPointerExit += OnPointerExitedTile;
        tileableComponent.OnPointerEnter += OnPointerEnteredTile;
        m_tilesMatrixGO[rowIdx, columnIdx] = tileGO;
    }
    #endregion

    #region Callbacks
    //Callbacks used on tiles initialization to check if there is an input over it
    private void OnPointerEnteredTile(int rowIdx, int columnIdx)
    {
        if (m_onPointerEnteredTile != null)
            m_onPointerEnteredTile(rowIdx, columnIdx);
    }

    private void OnPointerExitedTile(int rowIdx, int columnIdx)
    {
        if (m_onPointerExitedTile != null)
            m_onPointerExitedTile(rowIdx, columnIdx);
    }

    //Callbacks called when an item or full board is refilled, called by objectMover, so when movement is finished
    private void OnItemRefilled(int rowIdx, int columnIdx)
    {
        if (m_onItemRefilled != null)
            m_onItemRefilled(rowIdx, columnIdx);
    }

    private void OnBoardFullRefilled()
    {
        if (m_onBoardFullRefilled != null)
            m_onBoardFullRefilled();
    }
    #endregion
}