﻿/// <summary>
/// Central class for game, manage game flux
/// </summary>
/// <remarks>
/// This class is used to manage game flux and starting point for the game , this is persistend over scenes, this is strictly linked to game
/// </remarks>

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : Singleton<GameManager> {

    [SerializeField] GameObject m_jewel;
    [SerializeField] PoolInitializer m_jewelPoolInitializer;
    [SerializeField] Timer m_timer;

    GameBoardLogic m_gameBoardLogic;
    ObjectsPool<GameObject, GameObjectsPoolAllocator> m_jewelPool;

    #region Initialization
    // Use this for initialization
    void Start ()
    {
        InitializeUI();
        //This is persistent over scene so we need to know when scene is laded
        SceneManager.sceneLoaded += SceneLoaded;
    }

    private void InitializeUI()
    {
        //Link to Ui events 
        UIManager.Instance.m_onInGameMenuExitButtonPressed += InGameExitButtonPressed;
        UIManager.Instance.m_onMainMenuEnterButtonPressed += MainMenuEnterButtonPressed;
        UIManager.Instance.m_onMainMenuExitButtonPressed += MainMenuExitButtonPressed;
        //This is here because this is a central point for scene loaded and application start
        AudioManager.Instance.Play(AudioManager.Instance.AudioClips.BackgroundMenu);
    }

    //If game scene is loaded, initialiize board
    private void InitializeBoard()
    {
        //We use a pool
        m_jewelPool = m_jewelPoolInitializer.CreatePool();

        //Create 8 x 8 board
        m_gameBoardLogic = new GameBoardLogic();
        m_gameBoardLogic.InitializeGame(m_jewel, StaticConf.Board.BoardRowsCount, StaticConf.Board.BoardColumnsCount, m_jewelPool);

        //Start the timer before game start and then start the game
        m_timer.m_onTimerChanged -= OnInGameTimeChanged;
        m_timer.m_onTimerChanged += OnFullScreenTimeChanged;
        m_timer.Play(StaticConf.Timer.TimerBeforeStart, StartGame);

        UIManager.Instance.SetFullScreenTimerActive(true);
    }

    //Start game after timer 5 sec
    void StartGame()
    {
        UIManager.Instance.SetFullScreenTimerActive(false);

        //Link to event to update score UI
        m_gameBoardLogic.m_onScoreGained += OnScoreGained;

        //New timer, for the game, 60 sec
        m_timer.m_onTimerChanged -= OnFullScreenTimeChanged; //remove event timer for starting game
        m_timer.m_onTimerChanged += OnInGameTimeChanged;
        m_timer.Play(StaticConf.Timer.TimerGame, OnGameTimerExpired);

        m_gameBoardLogic.StartGame();

        AudioManager.Instance.Play(AudioManager.Instance.AudioClips.BackgroundGame);
    }
    #endregion

    #region Callbacks
    //Scene loaded callback so we can perform right operations
    private void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        UIManager.Instance.SceneLoaded();
        if (scene.name == StaticConf.Scene.GameSceneName)
        {
            InitializeBoard();
        }
        else if (scene.name == StaticConf.Scene.MainMenuSceneName)
        {
            InitializeUI();
        }
    }

    //These callbacks to choose what to do when an event occured, 
    //we can use same modules (timer, ui buttons) and choose here what to do, maybe game flux can change
    private void MainMenuExitButtonPressed()
    {
        Application.Quit();
    }

    private void MainMenuEnterButtonPressed()
    {
        SceneManager.LoadScene(StaticConf.Scene.GameSceneName);
    }

    private void InGameExitButtonPressed()
    {
        SceneManager.LoadScene(StaticConf.Scene.MainMenuSceneName);
        m_timer.Stop(false);
    }

    void OnGameTimerExpired()
    {
        m_gameBoardLogic.EndGame();
        UIManager.Instance.SetGameOverActive(true);
    }

    void OnScoreGained(int currentScore)
    {
        UIManager.Instance.UpdateScoreUI(currentScore);
    }

    private void OnInGameTimeChanged(float value)
    {
        UIManager.Instance.UpdateInGameTimer(value);
    }

    private void OnFullScreenTimeChanged(float value)
    {
        UIManager.Instance.UpdateFullScreenTimer(value);
    }
    #endregion
}
