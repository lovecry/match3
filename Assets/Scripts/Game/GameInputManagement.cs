﻿/// <summary>
/// Class for In game input
/// </summary>
/// <remarks>
/// This class is used to manage in game Input, using and wrapping input structure
/// </remarks>

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameInputManagement {

    //Possibily direction in this game
    public enum InputDirection
    {
        UP,
        RIGHT,
        BOTTOM,
        LEFT
    }

    //Public event for in game input detected
    public event System.Action<TileableItemBase> m_onHoveringTile;
    public event System.Action<InputDirection> m_onInputDetected;
    public event System.Action m_onTileableItemSelected;

    Vector2 m_inputStartingPoint = Vector2.zero;
    Vector2 m_inputEndingPoint = Vector2.zero;
    bool m_isInputting = false;

    //This bool is used to block input if needed
    bool m_inputBlocked = false;
    public bool SetInputBlocked { get { return m_inputBlocked; } set { m_inputBlocked = value; } }

    #region Initilaization
    public GameInputManagement()
    {
        InputManager.Instance.m_onInputStarted += OnInputStarted;
        InputManager.Instance.m_onInputEnded += OnInputEnded;
        BoardManager.Instance.m_onPointerExitedTile += PointerExitedTile;
        BoardManager.Instance.m_onPointerEnteredTile += PointerEnteredTile;
    }
    #endregion

    #region Callbacks
    private void OnInputStarted(Vector2 startingPoint)
    {
        if (!m_inputBlocked)
        {
            m_inputStartingPoint = startingPoint;
            m_isInputting = true;
            if (m_onTileableItemSelected != null)
                m_onTileableItemSelected();
        }
    }

    private void OnInputEnded(Vector2 endingPoint)
    {
        if (!m_inputBlocked)
        {
            m_inputEndingPoint = endingPoint;
            m_isInputting = false;
        }
    }

    private void PointerEnteredTile(int row, int column)
    {
        if (!m_inputBlocked)
        {
            if (m_onHoveringTile != null)
                m_onHoveringTile(BoardManager.Instance.GetTile(row, column).GetComponent<TileableItemBase>());
        }
    }

    private void PointerExitedTile(int rowIdx, int columnIdx)
    {
        if (!m_inputBlocked)
        {
            if (m_isInputting)
            {
                //In this game, when exit tile, we force input end
                InputManager.Instance.ForceEndInput();
                CheckDirection();
            }
            else
            {
                if (m_onHoveringTile != null)
                    m_onHoveringTile(null);
            }
        }
    }
    #endregion

    //In this game we allow only 4 direction of input
    private void CheckDirection()
    {
        Vector2 direction = m_inputEndingPoint - m_inputStartingPoint;
        float distance = direction.magnitude;
       
        if (distance == 0)
        {
            //No input found, user clicked
            return;
        }

        direction.Normalize();

        if (Mathf.Abs(direction.x) > Mathf.Abs(direction.y))
        {
            if (direction.x > 0)
            {
                InputDetected(InputDirection.RIGHT);
            }
            else
            {
                InputDetected(InputDirection.LEFT);
            }
        }
        else
        {
            if (direction.y > 0)
            {
                InputDetected(InputDirection.UP);
            }
            else
            {
                InputDetected(InputDirection.BOTTOM);
            }
        }
    }

    void InputDetected(InputDirection inputDirection)
    {
        if (m_onInputDetected != null)
            m_onInputDetected(inputDirection);
    }
}
