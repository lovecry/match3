﻿/// <summary>
/// Class for match check
/// </summary>
/// <remarks>
/// This class provide useful methods to check if matches on board
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public static class MatchChecker
{
    //Core method for check
    static List<Jewel> FindMatches(int startX, int startY, Vector2 searchDir, int minMatchLength)
    {
        List<Jewel> matchesJewels = new List<Jewel>();
        Jewel startingJewel = null;

        //Diagonal search not allowed
        Assert.Test(searchDir.x == 0 || searchDir.y == 0, "Can't search in diaogonal way");

        //Protection if want check random row column
        if (!BoardManager.Instance.IsInsideTheBoard(startX, startY))
        {
            return null;
        }

        GameObject startingObject = BoardManager.Instance.GetTile(startX, startY);
        if (startingObject != null) //Get the item
        {
            startingJewel = startingObject.GetComponent<Jewel>();
            Assert.Test(startingJewel != null, " Tile in board not have Jewel component");
            matchesJewels.Add(startingJewel);
        }
        else //There is an hole in the board, return
        {
            return null;
        }

        //Search limit
        int maxSearchIterations = BoardManager.Instance.RowsCount > BoardManager.Instance.ColumnsCount ? BoardManager.Instance.RowsCount : BoardManager.Instance.ColumnsCount;

        for (int i = 1; i < maxSearchIterations - 1; ++i)
        {
            int nextX = startX + (int)Mathf.Clamp(searchDir.x, -1, 1) * i;
            int nextY = startY + (int)Mathf.Clamp(searchDir.y, -1, 1) * i;

            if (!BoardManager.Instance.IsInsideTheBoard(nextX, nextY)) //Checking outside the board
            {
                break;
            }

            //Get the next jewel
            Jewel nextJewelComponent = null;
            GameObject nextJewel = BoardManager.Instance.GetTile(nextX, nextY);
            if (nextJewel != null) // checkin a null tile
            {
                nextJewelComponent = nextJewel.GetComponent<Jewel>();
            }
            else
            {
                break;
            }

            //Check if there is a match, if no, break iteration
            if (nextJewelComponent.Color == startingJewel.Color && !matchesJewels.Contains(nextJewelComponent)) 
            {
                matchesJewels.Add(nextJewelComponent);
            }
            else
            {
                break;
            }
        }

        //For the last, return jewels only if match reached min number
        if (matchesJewels.Count >= minMatchLength)
        {
            return matchesJewels;
        }
        return null;
    }

    //Find only vertical matches
    public static List<Jewel> FindVerticalMatches(int startX, int startY, int minLength = 3)
    {
        //Check top with core method
        List<Jewel> upwardMatches = FindMatches(startX, startY, Vector2.up, 2);
        // check bottom
        List<Jewel> downwardMatches = FindMatches(startX, startY, Vector2.down, 2);

        if (upwardMatches == null)
        {
            upwardMatches = new List<Jewel>();
        }

        if (downwardMatches == null)
        {
            downwardMatches = new List<Jewel>();
        }

        //Merge matches found, use Linq to prevent double occurency
        var combinedMatches = upwardMatches.Union(downwardMatches).ToList();

        //Return matches only if reached min number
        return (combinedMatches.Count >= minLength) ? combinedMatches : null;
    }

    //Same of vertical matches but horizontal
    public static List<Jewel> FindHorizontalMatches(int startX, int startY, int minLength = 3)
    {
        List<Jewel> rightMatches = FindMatches(startX, startY, Vector2.right, 2);
        List<Jewel> leftMatches = FindMatches(startX, startY, Vector2.left, 2);

        if (rightMatches == null)
        {
            rightMatches = new List<Jewel>();
        }

        if (leftMatches == null)
        {
            leftMatches = new List<Jewel>();
        }

        var combinedMatches = rightMatches.Union(leftMatches).ToList();

        return (combinedMatches.Count >= minLength) ? combinedMatches : null;

    }

    //Find match at specified position, this method use vertical and horizontal find methods (above)
    public static List<Jewel> FindMatchesAt(int x, int y, int minLength = 3)
    {
        List<Jewel> horizMatches = FindHorizontalMatches(x, y, minLength);
        List<Jewel> vertMatches = FindVerticalMatches(x, y, minLength);

        if (horizMatches == null)
        {
            horizMatches = new List<Jewel>();
        }

        if (vertMatches == null)
        {
            vertMatches = new List<Jewel>();
        }
        var combinedMatches = horizMatches.Union(vertMatches).ToList();
        return combinedMatches;
    }

    //Overload method to find matches of a list of Jewels
    public static List<Jewel> FindMatchesAt(List<Jewel> Jewels, int minLength = 3)
    {
        List<Jewel> matches = new List<Jewel>();

        foreach (Jewel piece in Jewels)
        {
            matches = matches.Union(FindMatchesAt(piece.RowIdx, piece.ColumnIdx, minLength)).ToList();
        }

        return matches;
    }

    //Find Matches in full board positions and combine results,
    public static List<Jewel> FindAllMatches()
    {
        List<Jewel> combinedMatches = new List<Jewel>();

        for (int i = 0; i < BoardManager.Instance.RowsCount; i++)
        {
            for (int j = 0; j < BoardManager.Instance.ColumnsCount; j++)
            {
                List<Jewel> matches = FindMatchesAt(i, j);
                combinedMatches = combinedMatches.Union(matches).ToList();
            }
        }
        return combinedMatches;
    }
}
