﻿/// <summary>
/// Class for static configuration
/// </summary>
/// <remarks>
/// This class is used to store statical configuration and to have a central point if want to modify them, all magic numbers shold stay here
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StaticConf {

    public class Scene
    {
        public static readonly string MainMenuSceneName = "MainMenu";
        public static readonly string GameSceneName = "Game";
    }

    public class Board
    {
        public const int BoardRowsCount = 8;
        public const int BoardColumnsCount = 8;
    }

    public class Tiles
    {
        public static readonly Vector2 TilesSpacing = new Vector2(0.1f, 0.1f);
    }

    public class Timer
    {
        public const float TimerBeforeStart = 5.0f;
        public const float TimerGame = 60.0f;
    }
}
