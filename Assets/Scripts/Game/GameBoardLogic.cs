﻿/// <summary>
/// Class for manage in game Logic
/// </summary>
/// <remarks>
/// This class is used to manage in game logic, strictly linked to game
/// </remarks>

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBoardLogic {

    //Public event to allow UI update and other check based on score
    public event System.Action<int> m_onScoreGained;

    GameInputManagement m_gameInput = null;

    TileableItemBase m_currentHoveringObject = null;
    TileableItemBase m_selectedTile = null;

    bool m_itemsAreSwapping = false;
    int m_boardRowCount = 0;
    int m_boardColumnCount = 0;
    int m_totalScore = 0;

    #region Initialization
    public void InitializeGame(GameObject jewel, int boardRowCount, int boardColumnCount, ObjectsPool<GameObject, GameObjectsPoolAllocator> objectPool)
    {
        m_boardRowCount = boardRowCount;
        m_boardColumnCount = boardColumnCount;

        BoardManager.Instance.GenerateBoard(m_boardRowCount, m_boardColumnCount, StaticConf.Tiles.TilesSpacing, jewel);
        BoardManager.Instance.m_onItemRefilled += OnItemRefilled;

        m_gameInput = new GameInputManagement();
        m_gameInput.m_onHoveringTile += OnTileHovered;
        m_gameInput.m_onInputDetected += OnSwapDetected;
        m_gameInput.m_onTileableItemSelected += OnItemSelected;
        m_gameInput.SetInputBlocked = true;

        foreach (GameObject jewelObj in BoardManager.Instance.Tiles)
        {
            jewelObj.GetComponent<Jewel>().SetColor((Jewel.ColorType)UnityEngine.Random.Range(0, System.Enum.GetNames(typeof(Jewel.ColorType)).Length - 1));
        }
        OnBoardRefilled();
    }
    #endregion

    #region public Interface
    public void StartGame()
    {
        m_gameInput.SetInputBlocked = false;
    }

    public void EndGame()
    {
        m_gameInput.SetInputBlocked = true;
    }
    #endregion

    //Remove items on board (Match) and refill 
    void RemoveItemAndRefillBoard(List<Jewel> itemsToRemove)
    {
        //Play one time FX on remove
        if (itemsToRemove.Count > 0)
        {
            AudioManager.Instance.PlayOneShot(AudioManager.Instance.AudioClips.JewelDestruction);
        }
        for (int i = 0; i < itemsToRemove.Count; ++i)
        {
            if (!itemsToRemove[i].IsDestroying) //If jewel already destroying (for the animation delay), ignore it
            {
                //Copy of index for lambda function
                int itemToRemoveIdx = i;
                //Start destroy animation and....
                if (i != itemsToRemove.Count - 1) //...then destroy item
                {
                    itemsToRemove[i].StartDestroyAnimation(() =>
                    {
                        RemoveTileFromBoard(itemsToRemove[itemToRemoveIdx]);
                    });
                }
                else // then destroy item and collapse and refill board
                {
                    itemsToRemove[i].StartDestroyAnimation(() =>
                    {
                        RemoveTileFromBoard(itemsToRemove[itemToRemoveIdx]);
                        CollapseAndRefillBoard();
                    });
                }
            AddScore();
            }
        }
    }

    //Remove a single jewel to board
    void RemoveTileFromBoard(Jewel tileToRemove)
    {
        BoardManager.Instance.RemoveTileFromBoard(tileToRemove.RowIdx, tileToRemove.ColumnIdx);
    }

    //Refill board and initialize new jewels
    void CollapseAndRefillBoard()
    {
        BoardManager.Instance.CollapseBoard();
        List<GameObject> itemsForRefill = BoardManager.Instance.RefillBoard();
        for (int i = 0; i < itemsForRefill.Count; ++i)
        {
            itemsForRefill[i].GetComponent<Jewel>().SetColor((Jewel.ColorType)UnityEngine.Random.Range(0, System.Enum.GetNames(typeof(Jewel.ColorType)).Length - 1));
        }
    }

    //Is item implementing movable interface? or already moving?
    bool IsItemMovable(int rowIdx, int columnIdx)
    {
        if (!BoardManager.Instance.IsInsideTheBoard(rowIdx, columnIdx) || BoardManager.Instance.GetTile(rowIdx, columnIdx) == null)
        {
            return false;
        }
        IMovableObject movableComponentItem = BoardManager.Instance.GetTile(rowIdx, columnIdx).GetComponent<IMovableObject>();
        Jewel jewelComponent = BoardManager.Instance.GetTile(rowIdx, columnIdx).GetComponent<Jewel>();

        return movableComponentItem != null && jewelComponent != null ? !movableComponentItem.IsMoving && !jewelComponent.IsDestroying : false;
    }

    void AddScore()
    {
        m_totalScore++;
        if (m_onScoreGained != null)
            m_onScoreGained(m_totalScore);
    }

    #region Callbacks
    void OnTileHovered(TileableItemBase item)
    {
        m_currentHoveringObject = item;
    }

    //Callback on pointer down input to item, select it if there is one and it's a tileable item
    void OnItemSelected()
    {
        if (m_currentHoveringObject != null)
        {
            GameObject hoveringObj = BoardManager.Instance.GetTile(m_currentHoveringObject.RowIdx, m_currentHoveringObject.ColumnIdx);
            if (hoveringObj != null)
            {
                TileableItemBase tileSelected = hoveringObj.GetComponent<TileableItemBase>();
                m_selectedTile = tileSelected;
            }
            else
            {
                return;
            }
        }
    }

    //Input to move detected, swap Jewels
    void OnSwapDetected(GameInputManagement.InputDirection inputDirection)
    {
        if (m_selectedTile != null && !m_itemsAreSwapping) //Do not swap if items already swapping or no selected item
        {
            //Find 2nd swapping item row and column idx
            int item2ToSwapRowIdx = m_selectedTile.RowIdx + (inputDirection == GameInputManagement.InputDirection.UP ? 1 : inputDirection == GameInputManagement.InputDirection.BOTTOM ? -1 : 0);
            int item2ToSwapColIdx = m_selectedTile.ColumnIdx + (inputDirection == GameInputManagement.InputDirection.RIGHT ? 1 : inputDirection == GameInputManagement.InputDirection.LEFT ? -1 : 0);

            //Check if items are movables
            if (IsItemMovable(m_selectedTile.RowIdx, m_selectedTile.ColumnIdx) && IsItemMovable(item2ToSwapRowIdx, item2ToSwapColIdx))
            {
                m_itemsAreSwapping = true;
                //Swap items on board manager
                BoardManager.Instance.SwapTiles(m_selectedTile.RowIdx, m_selectedTile.ColumnIdx, item2ToSwapRowIdx, item2ToSwapColIdx, OnSwapEnded);
                AudioManager.Instance.PlayOneShot(AudioManager.Instance.AudioClips.JewelSwap);
            }
            m_selectedTile = null;
        }
    }

    //Swap ended callback
    void OnSwapEnded(TileableItemBase item1, TileableItemBase item2)
    {
        m_selectedTile = null;
        m_itemsAreSwapping = false;
        //Pointer protection
        if (item1 == null || item2 == null)
        {
            return;
        }

        //Check for matches in row and column position
        List<Jewel> matches = MatchChecker.FindMatchesAt(item1.RowIdx, item1.ColumnIdx, 3);
        matches.AddRange(MatchChecker.FindMatchesAt(item2.RowIdx, item2.ColumnIdx, 3));

        if (matches.Count > 0) //If matches found, Remove items and refill board
        {
            RemoveItemAndRefillBoard(matches);
        }
        else //Otherwise, swap back items
        {
            m_itemsAreSwapping = true;
            BoardManager.Instance.SwapTiles(item1.RowIdx, item1.ColumnIdx, item2.RowIdx, item2.ColumnIdx, (TileableItemBase obj1, TileableItemBase obj2) => { m_itemsAreSwapping = false; });
            AudioManager.Instance.PlayOneShot(AudioManager.Instance.AudioClips.JewelSwap);
        }
    }

    //When item or board are refilled, check for matches that could be generated from thet refill
    private void OnItemRefilled(int rowIdx, int columnIdx)
    {
        RemoveItemAndRefillBoard(MatchChecker.FindMatchesAt(rowIdx, columnIdx));
    }

    void OnBoardRefilled()
    {
        RemoveItemAndRefillBoard(MatchChecker.FindAllMatches());
    }
    #endregion
}
