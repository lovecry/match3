﻿/// <summary>
/// Class for In Game UI
/// </summary>
/// <remarks>
/// This class is used to manage In Game UI Events (like exit butto, timer text and score text)
/// </remarks>

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InGameUI : MonoBehaviour {

    [SerializeField] Button m_exitGameButton;
    [SerializeField] Text m_timerText;
    [SerializeField] Text m_scoreText;
    [SerializeField] GameObject m_timerBeforeStartContainer;
    [SerializeField] Text m_timerBeforeStartText;
    [SerializeField] GameObject m_gameOverContainer;

    System.Action m_onExitGameButtonPressed;

    #region initialization
    private void Start()
    {
        m_exitGameButton.onClick.AddListener(OnExitButtonPressed);
    }

    public void Initialize(System.Action onExitGameButtonPressed)
    {
        m_onExitGameButtonPressed = onExitGameButtonPressed;
    }
    #endregion

    #region Update UI elements
    public void SetTimerText(float timeSeconds)
    {
        m_timerText.text = ((int)timeSeconds).ToString();
    }

    //Show timer in seconds
    public void SetTimerBeforeStartText(float timeSeconds)
    {
        m_timerBeforeStartText.text = ((int)timeSeconds).ToString();
    }

    public void SetTimerFullScreenActive(bool value)
    {
        m_timerBeforeStartContainer.SetActive(value);
    }

    public void SetScore(int totalPoints)
    {
        m_scoreText.text = totalPoints.ToString();
    }

    public void SetGameOverActive(bool value)
    {
        m_gameOverContainer.SetActive(value);
    }
    #endregion

    #region Callbacks
    private void OnExitButtonPressed()
    {
        AudioManager.Instance.PlayOneShot(AudioManager.Instance.AudioClips.ButtonPressed);
        if (m_onExitGameButtonPressed != null)
            m_onExitGameButtonPressed();
    }
    #endregion
}
