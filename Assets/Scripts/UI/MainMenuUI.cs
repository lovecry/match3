﻿/// <summary>
/// Class for Main Menu UI
/// </summary>
/// <remarks>
/// This class is used to manage Main Menu UI Events (like exit butto, play game button)
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenuUI : MonoBehaviour {

    [SerializeField] Button m_exitGameButton;
    [SerializeField] Button m_enterGameButton;

    System.Action m_onExitButtonPressed = null;
    System.Action m_onEnterButtonPressed = null;

    #region Initialization
    void Start()
    {
        m_exitGameButton.onClick.AddListener(OnExitGameButtonPressed);
        m_enterGameButton.onClick.AddListener(OnEnterGameButtonPressed);
    }

    public void Initialize(System.Action onExitButtonPressed, System.Action onEnterButtonPressed)
    {
        m_onExitButtonPressed = onExitButtonPressed;
        m_onEnterButtonPressed = onEnterButtonPressed;
    }
    #endregion

    #region Callbakcs
    void OnExitGameButtonPressed()
    {
        AudioManager.Instance.PlayOneShot(AudioManager.Instance.AudioClips.ButtonPressed);
        if (m_onExitButtonPressed != null)
            m_onExitButtonPressed();
    }

    void OnEnterGameButtonPressed()
    {
        AudioManager.Instance.PlayOneShot(AudioManager.Instance.AudioClips.ButtonPressed);
        if (m_onEnterButtonPressed != null)
            m_onEnterButtonPressed();
    }
    #endregion
}
