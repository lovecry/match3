/// <summary>
/// Assert class
/// </summary>
/// <remarks>
/// Useful class to launch assert when developing, and warning too
/// </remarks>

using UnityEngine;

public class Assert
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void Test (bool comparison, string message)
	{
		if (!comparison) 
		{
			Debug.LogError (message);
			Debug.Break();
		}
	}

	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	public static void Throw (string message)
	{
		Debug.LogWarning (message);
	}
}
