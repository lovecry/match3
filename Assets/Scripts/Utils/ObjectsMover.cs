﻿/// <summary>
/// Move your gameobject component or swap two GO
/// </summary>
/// <remarks>
/// This class is used to move you gameobject to a position, if add same gameobject, the old one is updated
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectsMover : MonoBehaviour {

    //Data structure for moving gameobject
    class MovingObject
    {
        public GameObject GameObjectToMove;
        public Vector3 DestinationPosition;
        public float MovementSpeed;
        public float DelayTime;
        public IMovableObject IMovableComponent;
        public System.Action OnObjectMovementEnded;
    }

    [SerializeField] float m_inBoardMovementSpeed = 8.0f;
    [SerializeField] float m_itemsSwapSpeed = 4.0f;

    //Move
    List<MovingObject> m_movingTileList = new List<MovingObject>(); //List of moving gameobject
    bool m_objectsAreMoving = false; //state for movement
    public event System.Action m_onAllMovementEnded = null; //callback when no gameobject is moving anymore

    //Swap
    MovingObject[] m_swappingTiles = new MovingObject[2]; //store reference of swapping gameobject
    bool m_objectsAreSwapping = false; //state for swapping
    System.Action m_onSwapEnded = null; //callback for swap ended

    #region public interface
    //Swap two object
    public void SwapTwoObjects(GameObject obj1, GameObject obj2, System.Action onSwapEnded = null)
    {
        if (obj1 == null || obj2 == null) //If no item to swap, return
        {
            return;
        }
        //Fill array references and start move obj
        m_swappingTiles[0] = CreateMoveObject(obj1, obj2.transform.position, m_itemsSwapSpeed, 0.0f, 1.0f);
        m_swappingTiles[1] = CreateMoveObject(obj2, obj1.transform.position, m_itemsSwapSpeed, 0.0f, 1.0f);

        m_onSwapEnded = onSwapEnded;
        m_objectsAreSwapping = true;
    }

    //Move obj 
    public void MoveObj(GameObject objToMove, Vector3 destinationPosition, System.Action onMovementEnded = null, float speedMultiplier = 1.0f)
    {
        UpdateOrAddNewMovingItem(objToMove, destinationPosition, m_inBoardMovementSpeed, 0.0f, speedMultiplier, onMovementEnded);
    }

    //Or move with delay
    public void MoveObjAfterDelay(GameObject objToMove, Vector3 destinationPosition, float delayTime, System.Action onMovementEnded = null, float speedMultiplier = 1.0f)
    {
        UpdateOrAddNewMovingItem(objToMove, destinationPosition,m_inBoardMovementSpeed, delayTime, speedMultiplier, onMovementEnded);
    }
    #endregion

    //Add moving item to list reference or update if already exist
    void UpdateOrAddNewMovingItem(GameObject objToMove, Vector3 destinationPosition, float movementSpeed, float delayTime, float speedMultiplier = 1.0f, System.Action onMovementEnded = null)
    {
        bool objAlreadyMoving = false;
        //Check if there is same gameobject 
        for (int i = 0; i < m_movingTileList.Count; ++i)
        {
            if (m_movingTileList[i].GameObjectToMove == objToMove)
            {
                //If is found, update it
                m_movingTileList[i].DestinationPosition = destinationPosition;
                m_movingTileList[i].MovementSpeed = movementSpeed * speedMultiplier;
                m_movingTileList[i].DelayTime = delayTime;
                m_movingTileList[i].OnObjectMovementEnded = onMovementEnded;
                objAlreadyMoving = true;
            }
        }
        if (!objAlreadyMoving)
        {
            //If not found, add it to moving object list
            m_movingTileList.Add(CreateMoveObject(objToMove, destinationPosition, m_inBoardMovementSpeed, delayTime, speedMultiplier, onMovementEnded));
        }
    }

    //Create a data structure for moving object and fill with all the info needed
    MovingObject CreateMoveObject(GameObject objToMove, Vector3 destinationPosition, float movementSpeed, float delayTime, float speedMultiplier = 1.0f, System.Action onMovementEnded = null )
    {
        if (objToMove == null)
        {
            //If no go, retrun
            return null;
        }

        //Create a data structure used to move objects
        MovingObject movingObj = new MovingObject();
        movingObj.GameObjectToMove = objToMove;
        movingObj.DestinationPosition = destinationPosition;
        movingObj.MovementSpeed = movementSpeed * speedMultiplier;
        movingObj.DelayTime = delayTime ;
        movingObj.OnObjectMovementEnded = onMovementEnded;

        //If this object implement IMovableInterface, flag as "Moving"
        IMovableObject movableComponent = objToMove.GetComponent<IMovableObject>();
        if (movableComponent != null)
        {
            movingObj.IMovableComponent = movableComponent;
            movableComponent.IsMoving = true;
        }

        return movingObj;
    }

    // Update is called once per frame
    void Update()
    {
        //Swap movement
        if (m_swappingTiles[0] != null || m_swappingTiles[1] != null)
        {
            //move gameobject if they didn't reach position;
            if (!Move(m_swappingTiles[0]))
                m_swappingTiles[0] = null;

            if (!Move(m_swappingTiles[1]))
                m_swappingTiles[1] = null;
        }
        else if (m_objectsAreSwapping)
        {
            //No object to move but they were moving so callback of end swapping
            m_objectsAreSwapping = false;
            if (m_onSwapEnded != null)
                m_onSwapEnded();
        }

        //BoardMovement
        if (m_movingTileList.Count > 0)
        {
            //move gameobjects in list to destination position
            m_objectsAreMoving = true;
            for (int i = m_movingTileList.Count - 1; i > -1; --i)
            {
                if (!Move(m_movingTileList[i]))
                    m_movingTileList.RemoveAt(i);
            }
        }
        else if (m_objectsAreMoving)
        {
            //No more gameobject to move but they were moving so callback and stop movements 
            m_objectsAreMoving = false;
            if (m_onAllMovementEnded != null)
                m_onAllMovementEnded();
            return;
        }
    }

    //Called by Update, this method move the gameobject
    bool Move(MovingObject objectToMove)
    {
        if (objectToMove == null || objectToMove.GameObjectToMove == null)
        {
            //Pointer protection
            return false;
        }
        if (objectToMove.GameObjectToMove.transform.position != objectToMove.DestinationPosition)
        {
            //If not in destination position
            if (objectToMove.DelayTime > 0)
            {
                //If there is a timer, remove time from it
                objectToMove.DelayTime -= Time.deltaTime;
            }
            else
            {
                //move obj
                objectToMove.GameObjectToMove.transform.position = Vector3.MoveTowards(objectToMove.GameObjectToMove.transform.position, objectToMove.DestinationPosition, Time.deltaTime * objectToMove.MovementSpeed);
            }
            return true;
        }
        else
        {
            //Item already at destination position, flag as "Not Moving" and call personal callback
            if (objectToMove.IMovableComponent != null)
            {
                objectToMove.IMovableComponent.IsMoving = false;
            }
            if (objectToMove.OnObjectMovementEnded != null)
            {
                objectToMove.OnObjectMovementEnded();
            }
            return false;
        }
    }
}
