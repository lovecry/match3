﻿/// <summary>
/// Show text over sprite
/// </summary>
/// <remarks>
/// This class to show text over a sprite, used for debug.
/// </remarks>

using UnityEngine;

public class SpriteText : MonoBehaviour
{
    void Start()
    {
        var parent = transform.parent;

        Renderer parentRenderer = parent.GetComponent<Renderer>();
        Renderer renderer = GetComponent<Renderer>();
        renderer.sortingLayerID = parentRenderer.sortingLayerID;
        renderer.sortingOrder = parentRenderer.sortingOrder;

        Transform spriteTransform = parent.transform;
        TextMesh text = GetComponent<TextMesh>();
        Vector3 pos = spriteTransform.position;
        text.text = string.Format("{0}, {1}", pos.x, pos.y);
    }
}