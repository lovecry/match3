﻿using UnityEngine;
using System.Collections;

public class PoolInitializer : MonoBehaviour 
{
	[SerializeField] string m_poolId;
	[SerializeField] GameObject m_prefab;
	[SerializeField] LayerMask m_layer;
	[SerializeField] PoolOpt m_instantiatePolicy;
	[SerializeField] int m_size;
	[SerializeField] PoolActiveStatus m_activeOnInstantiate = PoolActiveStatus.NotActive;
	[SerializeField] PoolActiveStatus m_activeOnRelease = PoolActiveStatus.DontChange;
	[SerializeField] PoolActiveStatus m_activeOnReturn = PoolActiveStatus.DontChange;
	[Tooltip("If true, returned objects are set to null parent (SetParent(null, false)) to avoid leaving them unintentionally parented to a GameObject being destroyed. Settign it to false requires you to manually care about this aspect.")]
	[SerializeField] bool m_parentNullOnReturn = true;

	public string poolId { get { return m_poolId; } }
	public LayerMask layer { get { return m_layer; } }
	public PoolOpt instantiatePolicy { get { return m_instantiatePolicy; } }
	public int size { get { return m_size; } }
	
	public virtual ObjectsPool<GameObject, GameObjectsPoolAllocator> CreatePool()
	{
		GameObjectsPoolAllocator allocator = new GameObjectsPoolAllocator(m_prefab, m_layer, m_activeOnInstantiate, m_activeOnRelease, m_activeOnReturn, m_parentNullOnReturn);
		ObjectsPool<GameObject, GameObjectsPoolAllocator> pool = new ObjectsPool<GameObject, GameObjectsPoolAllocator>();
		pool.Init(allocator, Mathf.Max(0, m_size), m_instantiatePolicy);
		return pool;
	}
}
