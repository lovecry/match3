﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectsPoolGC<TObj, TAlloc> where TObj : Object where TAlloc : ObjectsPoolAllocator<TObj>
{
	ObjectsPool<TObj, TAlloc> m_pool;
	Dictionary<int, TObj> m_objs;

	public Dictionary<int, TObj>.Enumerator GetObjectsEnumerator() { return m_objs.GetEnumerator(); }

	public ObjectsPoolGC(ObjectsPool<TObj, TAlloc> i_pool)
	{
		m_pool = i_pool;
		m_objs = new Dictionary<int, TObj>(i_pool.Count);
	}

	public void AddToGC(ObjectsPool<TObj, TAlloc> pool, TObj obj)
	{
		Assert.Test(pool == m_pool, "ASSERT ObjectsPoolGC.AddToGC: using wrong gc, not valid pool bind." );
		if(pool == m_pool)
		{
			int hash = obj.GetHashCode();
			if(!m_objs.ContainsKey(hash))
			{
				m_objs.Add(hash, obj);
			}
		}
	}

	public void RemoveFromGC(TObj obj)
	{
		int hash = obj.GetHashCode();
		if(m_objs.ContainsKey(hash))
		{
			m_objs.Remove(hash);
		}
	}

	public void Collect(TObj obj, System.Action<TObj> collecting = null)
	{
		int hash = obj.GetHashCode();
		if(m_objs.ContainsKey(hash))
		{
			m_objs.Remove(hash);
			if(collecting != null) {
				collecting(obj);
			}
			m_pool.Destroy(obj);
		}
	}

	public void CollectAll(System.Action<TObj> collecting = null)
	{
		using(Dictionary<int, TObj>.Enumerator enumerator = m_objs.GetEnumerator())
		{
			while(enumerator.MoveNext())
			{
				if(collecting != null) {
					collecting(enumerator.Current.Value);
				}
				m_pool.Destroy(enumerator.Current.Value);
			}
		}
		m_objs.Clear();
	}

	public void CollectAllAndDisposePool()
	{
		CollectAll();
		m_pool.Dispose();
	}
}

public static class ObjectsPoolGCExtensions
{
	public static void DisposeComponentAndRemoveFromGC<T>(this ObjectsPoolGC<GameObject,GameObjectsPoolAllocator> instance, T component) where T: Component, System.IDisposable
	{
		System.IDisposable disposableComponent = component as System.IDisposable;
		Assert.Test(disposableComponent != null, "ASSERT ObjectsPoolGC.DisposeComponentAndRemoveFromGC: component doesn't implement System.IDisposable interface.");
		if(disposableComponent != null)
		{
			disposableComponent.Dispose();
		}
		instance.RemoveFromGC(component.gameObject);
	}

	public static void DisposeComponentAndCollect<T>(this ObjectsPoolGC<GameObject,GameObjectsPoolAllocator> instance, T component) where T: Component, System.IDisposable
	{
		System.IDisposable disposableComponent = component as System.IDisposable;
		Assert.Test(disposableComponent != null, "ASSERT ObjectsPoolGC.DisposeComponentAndCollect: component doesn't implement System.IDisposable interface.");
		if(disposableComponent != null)
		{
			disposableComponent.Dispose();
		}
		instance.Collect(component.gameObject);
	}

	public static void DisposeComponentsAndCollectAll<T>(this ObjectsPoolGC<GameObject,GameObjectsPoolAllocator> instance) where T: Component, System.IDisposable
	{
		// disposing components with IDisposable interface
		using(Dictionary<int, GameObject>.Enumerator iter = instance.GetObjectsEnumerator())
		{
			while(iter.MoveNext())
			{
				T component = iter.Current.Value.GetComponent<T>();
				Assert.Test(component != null, "ASSERT ObjectsPoolGC.DisposeComponentsAndCollect: gameobject in collection doesn't have requested component to dispose.");
				if(component != null)
				{
					System.IDisposable disposableComponent = component as System.IDisposable;
					Assert.Test(disposableComponent != null, "ASSERT ObjectsPoolGC.DisposeComponentsAndCollectAll: component doesn't implement System.IDisposable interface.");
					if(disposableComponent != null)
					{
						disposableComponent.Dispose();
					}
				}
			}
		}

		// collecting objects
		instance.CollectAll();
	}

	public static void DisposeComponentsAndCollectAllAndDisposePool<T>(this ObjectsPoolGC<GameObject,GameObjectsPoolAllocator> instance) where T: Component, System.IDisposable
	{
		instance.DisposeComponentsAndCollectAll<T>();
		instance.CollectAllAndDisposePool();
	}
}
