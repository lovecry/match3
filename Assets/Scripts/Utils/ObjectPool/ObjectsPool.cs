﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PrefabsPoolAllocator<T> : ObjectsPoolAllocator<T> where T: Component
{
	T resource;
	int resourceLayer; 
	bool setLayer = false;

	public PrefabsPoolAllocator(T res)
	{
		resource = res;
	}

	public PrefabsPoolAllocator(T res, int layer)
	{
		resource = res;
		resourceLayer = layer;
		setLayer = true;
	}

	public override T Instantiate()
	{
		T prefabInstance = GameObject.Instantiate(resource) as T;
		#if !NO_HIDE_POOLS
		prefabInstance.gameObject.hideFlags = HideFlags.HideInHierarchy;
		#endif
		prefabInstance.gameObject.SetActive(false);
		if (setLayer)
			prefabInstance.gameObject.layer = resourceLayer;
		return prefabInstance;
	}

	public override void Destroy (T prefabInstance)
	{
		GameObject.Destroy(prefabInstance.gameObject);
	}
}

public class GameObjectsPoolAllocator : ObjectsPoolAllocator<GameObject>
{
	protected Object resource = null;
	protected int resourceLayer; 
	protected bool setLayer = false;
	protected PoolActiveStatus activeInstantiate = PoolActiveStatus.NotActive;
	protected PoolActiveStatus activeRelease = PoolActiveStatus.DontChange;
	protected PoolActiveStatus activeReturn = PoolActiveStatus.DontChange;
	bool parentNullReturn = true;
	public GameObjectsPoolAllocator(Object res)
	{
		resource = res;
	}

	public GameObjectsPoolAllocator(Object res, int layer)
	{
		resource = res;
		resourceLayer = layer;
		setLayer = true;
	}

	public GameObjectsPoolAllocator(Object res, int layer, PoolActiveStatus activeOnInstantiate, PoolActiveStatus activeOnRelease, PoolActiveStatus activeOnReturn, bool parentNullOnReturn)
	{
		resource = res;
		resourceLayer = layer;
		setLayer = true;
		activeInstantiate = activeOnInstantiate;
		activeRelease = activeOnRelease;
		activeReturn = activeOnReturn;
		parentNullReturn = parentNullOnReturn;
	}

	public override GameObject Instantiate ()
	{
		GameObject obj = GameObject.Instantiate(resource) as GameObject;
		#if !NO_HIDE_POOLS
		obj.hideFlags = HideFlags.HideInHierarchy;
		#endif
		if(activeInstantiate != PoolActiveStatus.DontChange) {
			obj.SetActive(activeInstantiate == PoolActiveStatus.Active);
		}
		if (setLayer)
			obj.layer = resourceLayer;
		return obj;
	}

	public override void Destroy (GameObject obj)
	{
		GameObject.Destroy(obj);
	}

	public override void ReleaseFromPool (GameObject obj)
	{
		obj.hideFlags = 0;
		if(activeRelease != PoolActiveStatus.DontChange) {
			obj.SetActive(activeRelease == PoolActiveStatus.Active);
		}
	}

	public override void ReturnToPool (GameObject obj)
	{
		if(parentNullReturn)
		{
			obj.transform.SetParent(null, false);
		}
		#if !NO_HIDE_POOLS
		obj.hideFlags = HideFlags.HideInHierarchy;
		#endif
		if(activeReturn != PoolActiveStatus.DontChange) {
			obj.SetActive(activeReturn == PoolActiveStatus.Active);
		}
	}

	~GameObjectsPoolAllocator()
	{
		resource = null;
	}
}

public abstract class ObjectsPoolAllocator<TObj> where TObj : class
{
	public ObjectsPoolAllocator() {}
	public abstract TObj Instantiate();
	public abstract void Destroy(TObj obj);
	public virtual void ReleaseFromPool(TObj obj) {}
	public virtual void ReturnToPool(TObj obj) {}
}

public enum PoolActiveStatus
{
	DontChange = 0,
	NotActive,
	Active
}

public enum PoolOpt
{
	Rigid = 0, 
	Flexible, 
	FlexibleDealloc 
}

public static class ObjectsPoolExtensions
{
	public static T Instantiate<T>(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance) where T: Component
	{
		GameObject obj = instance.Instantiate();
		if(obj != null)
		{
			T component = obj.GetComponent<T>();
			if(component != null)
			{
				return component;
			}
			else
			{
				instance.Destroy(obj);
			}
		}
		return default(T);
	}

	public static ObjectsPoolGC<GameObject, GameObjectsPoolAllocator> CreateGC(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance)
	{
		return new ObjectsPoolGC<GameObject, GameObjectsPoolAllocator>(instance);
	}

	public static T Instantiate<T>(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance, ObjectsPoolGC<GameObject, GameObjectsPoolAllocator> gc) where T: Component
	{
		T component = Instantiate<T>(instance);
		gc.AddToGC(instance, component.gameObject);
		return component;
	}

	public static GameObject Instantiate(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance, ObjectsPoolGC<GameObject, GameObjectsPoolAllocator> gc)
	{
		GameObject obj = instance.Instantiate();
		gc.AddToGC(instance, obj);
		return obj;
	}

	public static void Destroy(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance, GameObject obj, ObjectsPoolGC<GameObject, GameObjectsPoolAllocator> gc)
	{
		gc.RemoveFromGC(obj);
		instance.Destroy(obj);
	}

	public static void Destroy<T>(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance, T component, ObjectsPoolGC<GameObject, GameObjectsPoolAllocator> gc) where T: Component
	{
		Destroy(instance, component.gameObject, gc);
	}

	public static void Destroy<T>(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance, T component) where T: Component
	{
		instance.Destroy(component.gameObject);
	}

	public static void DisposeComponentAndDestroy<T>(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance, T component) where T: Component, System.IDisposable
	{
		System.IDisposable disposableComponent = component as System.IDisposable;
		Assert.Test(disposableComponent != null, "ASSERT ObjectsPool.DisposeComponentAndDestroy: component doesn't implement System.IDisposable interface.");
		if(disposableComponent != null)
		{
			disposableComponent.Dispose();
		}
		Destroy<T>(instance, component);
	}

	public static void DisposeComponentAndDestroy<T>(this ObjectsPool<GameObject,GameObjectsPoolAllocator> instance, T component, ObjectsPoolGC<GameObject, GameObjectsPoolAllocator> gc) where T: Component, System.IDisposable
	{
		System.IDisposable disposableComponent = component as System.IDisposable;
		Assert.Test(disposableComponent != null, "ASSERT ObjectsPool.DisposeComponentAndDestroy: component doesn't implement System.IDisposable interface.");
		if(disposableComponent != null)
		{
			disposableComponent.Dispose();
		}
		Destroy(instance, component.gameObject, gc);
	}
}

public class ObjectsPool<TObj,TAlloc> where TObj : class where TAlloc : ObjectsPoolAllocator<TObj>
{
	Queue<TObj> pool = null;
	int size;
	TAlloc allocator;
	PoolOpt instatiateOpt = PoolOpt.Rigid;
	int overhead = 0;

	public bool initialized { get { return pool != null; } }

	public void Init(TAlloc alloc, int poolSize, PoolOpt opt)
	{
		if(!initialized && alloc != null)
		{
			allocator = alloc;
			size = Mathf.Max(0, poolSize);
			pool = new Queue<TObj>(size);
			instatiateOpt = opt;
			for(int i = 0; i < size; ++i)
			{
				TObj obj = allocator.Instantiate();
				pool.Enqueue(obj);
			}
		}
	}

	public void Dispose()
	{
		if(initialized)
		{
			//for(int i = 0; i < pool.Count; ++i)
			while(pool.Count > 0)
			{
				TObj obj = pool.Dequeue();
				allocator.Destroy(obj);
				obj = null;
			}
			pool.Clear();
			pool = null;
			allocator = null;
		}
	}

	public int Count { get { return pool.Count; } } // TODO: refactor available items
	public int Size { get { return size; } } 

	public TObj Instantiate()
	{
		if(initialized)
		{
			if(Count > 0)
			{
				TObj obj = pool.Dequeue();
				allocator.ReleaseFromPool(obj);
				return obj;
			}

			if(instatiateOpt != PoolOpt.Rigid) {
				TObj obj = allocator.Instantiate();
				allocator.ReleaseFromPool(obj);
				overhead++;
				return obj;
			}
		}

		return null;
	}

	public void Destroy(TObj obj)
	{
		if(initialized)
		{
			if(obj != null)
			{
				allocator.ReturnToPool(obj);
				if(instatiateOpt == PoolOpt.FlexibleDealloc && overhead > 0) {
					allocator.Destroy(obj);
					overhead--;
				}
				else {
					pool.Enqueue(obj);
				}
			}
		}
	}
}
/*
public class GameObjectsPool : ObjectsPool<GameObject, GameObjectsPoolAllocator>
{
	ObjectsPoolGC<GameObject, GameObjectsPoolAllocator> m_gc;

	public void Init(GameObjectsPoolAllocator alloc, int poolSize, PoolOpt opt, bool autoGC)
	{
		Init(alloc, poolSize, opt);
		if(autoGC)
		{
			m_gc = new ObjectsPoolGC<GameObject, GameObjectsPoolAllocator>(this);
		}
	}
}
*/