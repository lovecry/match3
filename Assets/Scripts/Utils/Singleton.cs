﻿/// <summary>
/// Singleton Pattern
/// </summary>
/// <remarks>
/// This class is used turn a class monobehaviour into singleton, allowing only one copy of it
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
{
    protected static T m_instance;
    public static T Instance
    {
        get
        {
            if (m_instance == null)
            {
                //Generate error if another instance of class is found.
                T[] instances = FindObjectsOfType<T>();
                Assert.Test(instances.Length == 1, "Another Istance of a singleton found");

                //Get the istance
                m_instance = instances[0]; 

                //If istance is null, create it
                if (m_instance == null)
                {
                    GameObject boardManager = new GameObject();
                    m_instance = boardManager.AddComponent<T>();
                    boardManager.name = typeof(T).ToString();
                }
            }
            return m_instance;
        }
    }
}
