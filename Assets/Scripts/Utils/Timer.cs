﻿/// <summary>
/// Class Timer
/// </summary>
/// <remarks>
/// Set a time and wait for callback or block it
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Timer : MonoBehaviour
{
    public event System.Action<float> m_onTimerChanged;

    [SerializeField] float m_timerSeconds = 60.0f;
    System.Action m_onTimerExpired;
    float m_currentRemainingTime = 0.0f;
    public float GetTime { get { return m_currentRemainingTime; } }

    #region Public Interface
    public void Play(float seconds, System.Action onTimerExpired)
    {
        m_currentRemainingTime = seconds;
        m_onTimerExpired = onTimerExpired;
        enabled = true;
    }

    public void Play(System.Action onTimerExpired = null)
    {
        m_currentRemainingTime = m_timerSeconds;
        m_onTimerExpired = onTimerExpired;
        enabled = true;
    }

    public void Stop(bool callCallbackTimeEnded)
    {
        if (callCallbackTimeEnded)
        {
            if (m_onTimerExpired != null)
                m_onTimerExpired();
        }
        m_currentRemainingTime = 0.0f;
        enabled = false;
    }
    #endregion

    private void Update()
    {
        if (m_currentRemainingTime <= 0)
        {
            m_currentRemainingTime = 0.0f;
            enabled = false;
            if (m_onTimerExpired != null)
                m_onTimerExpired();
        }
        else
        {
            m_currentRemainingTime -= Time.deltaTime;
            if (m_onTimerChanged != null)
                m_onTimerChanged(m_currentRemainingTime);
        }
    }

}
