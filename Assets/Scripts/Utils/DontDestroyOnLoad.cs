﻿/// <summary>
/// Do Not Destroy on load GameObject Component
/// </summary>
/// <remarks>
/// Use this component to prevent gameobject to destroy on level load and 
/// at same time prevent to create double copy on first level created is loaded again
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour {

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Destroy(gameObject);
        }
    }
}
