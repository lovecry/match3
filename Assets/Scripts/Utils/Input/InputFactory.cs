﻿/// <summary>
/// Input Factory
/// </summary>
/// <remarks>
/// Create the right Input system, available for now are touch or mouse but we can add here AI, Replay, Network...
/// </remarks>

using UnityEngine;
using System.Collections;

public class InputFactory 
{
	public static InputBase GetInput(InputManager.eInputSource eInputType)
	{
		InputBase oInputImplementation = null;

		switch(eInputType) 
		{
            //TODO : add here if more input sources are added into enum
            case InputManager.eInputSource.PLAYER:
			if(Input.touchSupported)
			{
				oInputImplementation = new InputPlayerTouchOneFinger();
			}
			else
			{
				oInputImplementation = new InputMouse();
			}
			break;
		};

		if(oInputImplementation == null)
		{
			Debug.LogError("Input implementation not available!");
		}

		return oInputImplementation;
	}
}
