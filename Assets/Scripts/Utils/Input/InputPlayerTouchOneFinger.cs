﻿/// <summary>
/// Input Mouse
/// </summary>
/// <remarks>
/// Specified Implementation for Touch Input
/// </remarks>

using UnityEngine;
using System.Collections;

public class InputPlayerTouchOneFinger : InputBase
{
    public float m_fTimeElapsed;

    #region Initialization
    public override void Init()
    {
        base.Init();
    }
    #endregion

    #region Public Interface
    public override void InputUpdate()
	{
		base.InputUpdate();

		if ( Input.touchCount > 0 )
		{
            Touch currentTouch = Input.touches[0];

			if(m_fTimeElapsed == 0.0f)
			{
				StartTouch(currentTouch);
			}
			else if (m_isInputting)
			{
                UpdateTouch(currentTouch);
			}
		}
		else
		{
            if (m_fTimeElapsed != 0.0f)
            {
                TouchFinished();
            }
		}
    }

    public override void ForceEndInput()
    {
        TouchFinished();
    }
    #endregion

    private void StartTouch(Touch touch)
	{
        m_fTimeElapsed = Time.time;
        m_startPosition = touch.position;
        UpdateTouch(touch);

        m_isInputting = true;

        InternalInputStarted(m_startPosition);
    }

    private void UpdateTouch(Touch touch)
    {
        m_endPosition = touch.position;
    }

	private void TouchFinished()
	{
        m_fTimeElapsed = 0.0f;
        m_isInputting = false;

        InternalInputEnded(m_endPosition);
    }
}