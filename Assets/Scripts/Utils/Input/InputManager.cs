﻿/// <summary>
/// Input Manager
/// </summary>
/// <remarks>
/// Manager Class for input structure, this class register input in specified input system and launch to outside
/// </remarks>

using UnityEngine;
using System.Collections;

public class InputManager : Singleton<InputManager>
{
    public enum eInputSource //TODO : add here more input sources aka replay, network, AI....
    {
        PLAYER = 0
    }

	[SerializeField] private eInputSource m_eInputSource = eInputSource.PLAYER;

    public event System.Action<Vector2> m_onInputStarted;
    public event System.Action<Vector2> m_onInputEnded;

    private InputBase m_oInput;

    void Awake()
    {
        m_instance = this;
    }

    #region Initialization
    void Start()
	{
		InitInput();
	}

    private void InitInput()
	{
		m_oInput = InputFactory.GetInput(m_eInputSource);
		
		if(m_oInput != null)
		{
			m_oInput.Init();
			m_oInput.Activate(InputStarted, InputEnded);
		}
	}
    #endregion

    #region Public Interface
    public void ForceEndInput()
    {
        if (m_oInput != null)
        {
            m_oInput.ForceEndInput();
        }
    }
    #endregion

    void Update()
    {
        if (m_oInput != null)
        {
            m_oInput.InputUpdate();
        }
    }

    #region Callbacks
    private void InputStarted(Vector2 startingPoint)
    {
        if (m_onInputStarted != null)
        {
            m_onInputStarted(startingPoint);
        }
    }

    private void InputEnded(Vector2 endingPoint)
    {
        if (m_onInputEnded != null)
        {
            m_onInputEnded(endingPoint);
        }
    }
    #endregion
}