﻿/// <summary>
/// Input Mouse
/// </summary>
/// <remarks>
/// Specified Implementation for Mouse Input
/// </remarks>

using UnityEngine;
using System.Collections;

public class InputMouse : InputBase
{
    #region Initialization
    public override void Init()
    {
        base.Init();
    }
    #endregion

    #region Public interface
    public override void InputUpdate()
	{
		base.InputUpdate();

        if (Input.GetMouseButtonDown(0) && !m_isInputting)
        {
            StartInput();
        }
        else if (Input.GetMouseButtonUp(0) && m_isInputting)
        {
            InputFinished();
        }
        else if (m_isInputting)
        {
            UpdateInput();
        }
	}
    
    public override void ForceEndInput()
    {
        InputFinished();
    }
    #endregion

    void StartInput()
    {
        m_startPosition = Input.mousePosition;
        m_isInputting = true;
        UpdateInput();

        InternalInputStarted(m_startPosition);
    }

    void UpdateInput()
    {
        m_endPosition = Input.mousePosition;
    }

    void InputFinished()
    {
        m_isInputting = false;
        InternalInputEnded(m_endPosition);
    }
}
