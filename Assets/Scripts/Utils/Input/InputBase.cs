﻿/// <summary>
/// Base Class for Input specified implementation
/// </summary>
/// <remarks>
/// Set a time and wait for callback or block it
/// </remarks>

using UnityEngine;
using System.Collections;

public class InputBase
{
    //Public events
    protected event System.Action<Vector2> m_OnInputStarted = null;
    protected event System.Action<Vector2> m_OnInputEnded = null;

    protected bool m_isInputting = false;
    protected Vector2 m_startPosition;
    protected Vector2 m_endPosition;

    public virtual void InputUpdate() { }
    public virtual void Init() { }
    public virtual void ForceEndInput() { }

    #region Public Interface
    public void Activate(System.Action<Vector2> onInputStarted, System.Action<Vector2> onInputEnded)
	{
        m_OnInputStarted = onInputStarted;
        m_OnInputEnded = onInputEnded;
    }

    public void Deactivate()
	{
        m_OnInputStarted = null;
        m_OnInputEnded = null;
	}
    #endregion

    #region Callback even
    protected void InternalInputStarted(Vector2 startingPoint)
    {
        if (m_OnInputStarted != null)
        {
            m_OnInputStarted(startingPoint);
        }
    }

    protected void InternalInputEnded(Vector2 endingPoint)
    {
        if (m_OnInputEnded != null)
        {
            m_OnInputEnded(endingPoint);
        }
    }
    #endregion
}
