﻿/// <summary>
/// Animate by reduce and Increase the size by scale
/// </summary>
/// <remarks>
/// This is a Gameobject Component, Set the configuration by code or by gameobject component
/// </remarks>

using UnityEngine;
using System.Collections;

public class ScaleAnimator : MonoBehaviour, IAnimator
{
    [SerializeField] AnimationCurve m_animationCurve;
    [SerializeField] Vector3 m_startingScale;
    [SerializeField] Vector3 m_targetScale;
    [SerializeField] float m_currentDuration;
    [SerializeField] bool m_loop = false;

    Transform m_objTransform;
    Vector3 m_originalScale;
    Vector3 m_originalStartingScale;
    Vector3 m_originalTargetScale;
    float m_timer;
    bool m_InvertedAnimation;
    System.Action m_onAnimationEnded; 


    void Awake()
    {
        //Don't need to Update if animator is not used
        enabled = false;
        Initialize();
    }

    public void SetConfiguration(AnimationCurve curve, Vector3 startingScale, Vector3 targetScale, float currentDuration, bool loop)
    {
        m_animationCurve = curve;
        m_startingScale = startingScale;
        m_targetScale = targetScale;
        m_currentDuration = currentDuration;
        m_loop = loop;
        Initialize();
    }

    void Initialize()
    {
        m_objTransform = GetComponent<Transform>();
        m_originalScale = m_objTransform.localScale;
        m_originalStartingScale = m_startingScale;
        m_originalTargetScale = m_targetScale;
    }

    public void Play(bool InverseOn, System.Action onAnimationEnded)
    {
        m_InvertedAnimation = InverseOn;
        m_onAnimationEnded = onAnimationEnded;
        if (InverseOn)
        {
            SwapTarget();
        }

        m_timer = 0.0f;
        enabled = true;
    }

    public void Stop(bool reset)
    {
        enabled = false;

        if (reset)
        {
            Reset();
        }
        else
        {
            m_objTransform.localScale = m_targetScale;
        }
    }

    public void Reset()
    {
        m_startingScale = m_originalStartingScale;
        m_targetScale = m_originalTargetScale;
        m_objTransform.localScale = m_originalScale;
    } 

	void Update()
	{
		m_timer += Time.deltaTime;

		AnimateScale ();
	}

	void AnimateScale()
	{
        //not perform animation if duration is 0 or below and prevent /0 crash
        if (m_currentDuration <= 0)
        {
            return;
        }
        m_objTransform.localScale = Vector3.Lerp (m_startingScale, m_targetScale, m_animationCurve.Evaluate (m_timer / m_currentDuration));
		
		if (m_timer > m_currentDuration)
		{
			m_timer = 0.0f;

			enabled = m_loop;
			if (m_InvertedAnimation)
			{
				SwapTarget ();
			}
            if (m_onAnimationEnded != null)
                m_onAnimationEnded();

        }
	}

	void SwapTarget()
	{
		Vector3 tmp = m_startingScale;
		m_startingScale = m_targetScale;
		m_targetScale = tmp;
	}
}
