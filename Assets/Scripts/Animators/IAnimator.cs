﻿/// <summary>
/// Interface for object animators
/// </summary>
/// <remarks>
/// Interface for class that want to animate things
/// </remarks>

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IAnimator {

    void Play(bool PlayInverse, System.Action onAnimationEnded);
    void Stop(bool Reset);
    void Reset();
}
